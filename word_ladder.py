#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 17:18:47 2019

@author: vaishnavi
"""
def compare_words(start, end):
    pairs = zip(start, end)
    change_list = []
    for x,y in pairs:
        change_list.append(x != y)
    return change_list

def is_valid_word(word, word_list):
    return word in word_list
    
def same_length_words(file_name, n):
    file = open(file_name,'r')
    content = file.readlines()
    word_list = []
    for word in content:
        if len(word[:-1]) == n:
            word_list.append(word[:-1])   
    return word_list
    
start_word = 'SALT'
end_word = 'GOLD'

print(compare_words('SALT','GOLD'))
print(is_valid_word('YYYY',same_length_words('sowpods.txt',4)))