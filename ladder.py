def gen_words_differ_by_1char(s: str) -> [str]:
    l = []
    for word in VALID_WORDS:
        d = 0
        for c1, c2 in zip(s, word):
            if c1 != c2:
                d += 1
        if d == 1:
            l.append(word)
    return l



def isValidWord(s: str) -> bool :
    return s in VALID_WORDS


VALID_WORDS =[word.strip() for word in open("sowpods.txt") if len(word.strip()) == 4]

import sys
s1 = sys.argv[1]
s2 = sys.argv[2]


for _ in build_ladder(s1, s2):
    print(_)